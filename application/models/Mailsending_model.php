<?php
/**
* Mailsending_Model Model for Admin and site
*/
class Mailsending_Model extends CI_Model {
	var $from = "";  
	var $sender_name = 'Cloudrobe';  
	var $sitelogo = '';
	var $site_title = '';
	var $contact_email = '';
	var $banner_image = '';
	var $mailtype = 'html';
	var $social_icons = '';
	 
	function __construct() {
		$this->load->library('email');
		$config['protocol'] = "smtp";
      	$config['smtp_host'] = getSiteOption('smtp_host',true);
      	$config['smtp_port'] = getSiteOption('smtp_port',true);
      	$config['smtp_user'] = getSiteOption('smtp_user',true);
      	$config['smtp_pass'] = getSiteOption('smtp_pass',true);      	
      	$config['charset'] = "utf-8";
      	$this->email->initialize($config);	
	}

	function templatechoose($slug) {
		$this->db->where('slug', $slug);
		$query = $this->db->get('tbl_emails');
		return $query->row();
	}

	function getUserRecordById($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_users');
		$result = $query->row();
		return $result;
	}

	function getMembersRecordById($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_members');
		$result = $query->row();
		return $result;
	}

	function getContactDetailsId($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_contact_us');
		$result = $query->row();
		return $result;
	}

	function sendEmail($emailAddress,$data,$subject) {
		$smtpUser = getSiteOption('smtp_user',true);
		$this->email->from($smtpUser,'Cloudrobe');
		$this->email->to($emailAddress);
		$this->email->mailtype = $this->mailtype;
		$this->email->set_newline("\r\n");
	    $this->email->subject($subject);
        $this->email->message($data);
        $this->email->send();        
	}

	function welcomeMailForZagaUser($id,$password) {
			
			$template = $this->templatechoose('welcome-mail-for-zaga-user');	
			$subject = $template->subject;	
			$userRecord = $this->getUserRecordById($id);	
			$name = $userRecord->first_name;
			$username = $userRecord->username;					
			$emailAddress = $userRecord->email;			
			$siteURL = '<a href="'.site_url().'">Visit Cloudrobe</a>';
			$logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/sport_logo.png" class="logo-default"/>';
			$str = $template->content;
			$str = str_replace('{{Logo}}', $logoURL, $str);		
			$str = str_replace('{{First_Name}}',$name, $str);
			$str = str_replace('{{Username}}',$username, $str);
			$str = str_replace('{{Password}}',$password, $str);
			$str = str_replace('{{Email}}',$emailAddress, $str);
			$str = str_replace('{{Website_URL}}', $siteURL, $str);			
			$str = str_replace('{{Subject}}', $subject, $str);			
			$this->sendEmail($emailAddress,$str,$subject);
	    
	}

	function sendEmailForNotifications($subject,$content,$user_ids) {
		foreach ($user_ids as $key => $value) {		
			$template = $this->templatechoose('send-email-notifications');	
			$replySubject = $template->subject;	
			$memberRecord = $this->getMembersRecordById($value);	
			$name = $memberRecord->first_name;					
			$emailAddress = $memberRecord->email;			
			$siteURL = '<a href="'.site_url().'">Visit Cloudrobe</a>';
			$logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/sport_logo.png" class="logo-default"/>';
			$str = $template->content;
			$str = str_replace('{{Logo}}', $logoURL, $str);		
			$str = str_replace('{{First_Name}}',$name, $str);
			$str = str_replace('{{Email_Address}}',$emailAddress, $str);
			$str = str_replace('{{Website_URL}}', $siteURL, $str);			
			$str = str_replace('{{Subject}}', $subject, $str);
			$str = str_replace('{{Message}}', $content, $str);
			$this->sendEmail($emailAddress,$str,$subject);
	    }
	}

	function replyContactQuery($replyMessage,$id) {
		$template = $this->templatechoose('reply-comment-query');
		$subject = $template->subject;
		$contactQuery = $this->getContactDetailsId($id);
		$name = 	$contactQuery->your_name;	
		$visitor_message = 	$contactQuery->message;	
		$replyMessage = $replyMessage;
		$emailAddress = $contactQuery->email;			
		$siteURL = '<a href="'.site_url().'">Visit Cloudrobe</a>';
		$logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/sport_logo.png" class="logo-default"/>';
		$str = $template->content;
		$str = str_replace('{{Logo}}', $logoURL, $str);
		$str = str_replace('{{Name}}', $name, $str);		
		$str = str_replace('{{Email_Address}}',$emailAddress, $str);
		$str = str_replace('{{Website_URL}}', $siteURL, $str);
		$str = str_replace('{{Visitor_query}}', $visitor_message, $str);		
		$str = str_replace('{{Reply_Message}}', $replyMessage, $str);
		$this->sendEmail($emailAddress,$str,$subject);
	}

	function forgotPasswordUserMailSending($userId, $forgotPasswordKey, $url) {
		$template = $this->templatechoose('forgot-password');
		$subject = $template->subject;
		$user = $this->getUserRecordById($userId);
		$firstName = $user->first_name;
		$lastName = $user->last_name;
		$emailAddress = $user->email;
		$siteURL = '<a href="'.site_url().'">Visit Cloudrobe</a>';
		$logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/sport_logo.png" class="logo-default"/>';
		$resetURL = $url;
		$str = $template->content;
		$str = str_replace('{{Logo}}', $logoURL, $str);
		$str = str_replace('{{First_Name}}', $firstName, $str);
		$str = str_replace('{{Last_Name}}', $lastName, $str);
		$str = str_replace('{{Email_Address}}', $emailAddress, $str);
		$str = str_replace('{{Website_URL}}', $siteURL, $str);
		$str = str_replace('{{Forgot_Password}}', $resetURL, $str);
		$this->sendEmail($emailAddress, $str, $subject);
	}

	function replySubcribeQuery($replyMessage,$ids) {
		foreach ($ids as $key => $value) {		
			$subscribeQuery = $this->getSubscribeRecordById($value);
			$template = $this->templatechoose('reply-subscribe-query');	
			$replySubject = $template->subject;			
			$emailAddress = $subscribeQuery->email;			
			$siteURL = '<a href="'.site_url().'">Visit Funrumble</a>';
			$logoURL = '<img src="'.$this->config->item('admin_assets').'global/img/sport_logo.png" class="logo-default"/>';
			$str = $template->content;
			$str = str_replace('{{Logo}}', $logoURL, $str);		
			$str = str_replace('{{Email_Address}}',$emailAddress, $str);
			$str = str_replace('{{Website_URL}}', $siteURL, $str);			
			$str = str_replace('{{Reply_Subject}}', $replySubject, $str);
			$str = str_replace('{{Reply_Message}}', $replyMessage, $str);
			$this->sendEmail($emailAddress,$str,$replySubject);
	    }
	}

	
	function getSubscribeRecordById($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('tbl_subscribers'); 
		$result = $query->row();
		return $result;
	}

}