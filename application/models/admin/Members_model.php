<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Members_model extends CI_Model
{
	
	function __construct() 
	{
	  parent::__construct();
	  $this->table = 'tbl_members';
	}

	function getAllrecord()
    {
      $this->db->select('*');
      $this->db->order_by('id','DESC');
      $query = $this->db->get($this->table);	;
      $result= $query->result();
      return $result;
    } 
    

	function getSingleRecordById($id) 
	{
	  $this->db->select('*');
      $this->db->order_by('id','DESC');
	  $this->db->where('id',$id);
	  $query = $this->db->get($this->table);	
	  $result = $query->row();
	  return $result;
	}
	
	
    function addNewRecord($data)
	{
	  $this->db->insert('tbl_members',$data);	
	  return $this->db->insert_id();	
	}
	
	function updateRecordById($id,$input)
	{
	  $this->db->where('id',$id);
	  $this->db->update('tbl_members',$input);
	}

	function deleteRecordById($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function changeStatusById($id,$status) 
	{
	  $this->db->where('id',$id);
	  $this->db->update($this->table,array('status' => $status));
	}

	function getAllActiveCategory() {
      $this->db->select('id,category_name');    
      $this->db->where('status','Active'); 
      $this->db->order_by('category_name','asc');
      $query = $this->db->get('tbl_members');
      $result= $query->result();
      return $result;
    }

    function countAllRecordsOnSite(){
		$this->db->select('id');	
		$query = $this->db->get($this->table);
		$result = $query->num_rows();
		return $result;
	}
	
}	