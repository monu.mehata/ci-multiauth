<?php
/**
* Static_page_model Model for Admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Static_page_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->table = 'tbl_static_page';
	}

	function getAllStaticPages()
	{
		$this->db->select('*');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

	function changeStatusById($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update($this->table,array('status' => $status));
	}

	function getSingleStaticPageById($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		$result = $query->row();
		return $result;
	}

	function updateStaticPage($id,$data)
	{
		$this->db->where('id',$id);
		$result = $this->db->update($this->table,$data);
		return $result;
	}
}