<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_queries_model extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->table = 'tbl_contact_us';
	}

	function getAllContactQueries()
	{
		$this->db->select('*');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}
	
	function updateContactQuery($id,$data)
	{
		$this->db->where('id',$id);
		$result = $this->db->update($this->table,$data);
		return $result;
	}

	function getSignleRecordById($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		$result = $query->row();
		return $result;
	}

	function deleteContactById($id)
	{
		$this->db->where('id',$id);
		$this->db->delete($this->table);
	}

   
   	function changeStatusById($id)
	{
		$this->db->where('id',$id);
		$this->db->update($this->table);
	}
}