<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Transactions_model extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->table = 'tbl_transactions';
	}

	function countAllRecords($searchData) {	
	    $this->db->select('tbl_transactions.*');
		$this->db->select('tbl_members.first_name,tbl_members.last_name');	
		if(isset($searchData['keyword']) && $searchData['keyword']) {
			$this->db->group_start();			
				$this->db->like('tbl_transactions.amount',$searchData['keyword']);
				$this->db->or_like('tbl_members.first_name',$searchData['keyword']);
				$this->db->or_like('tbl_members.last_name',$searchData['keyword']);
			$this->db->group_end();
		}	
		if(isset($searchData['sorting_order']) && $searchData['sorting_order']) {
			$this->db->order_by('tbl_transactions.id', $searchData['sorting_order']);
		}
		if(isset($searchData['date']) && $searchData['date']){			
			$this->db->like('tbl_transactions.add_date',$searchData['date']);
		}			
		$this->db->join('tbl_members','tbl_members.id = tbl_transactions.user_id','LEFT');
		$query = $this->db->get('tbl_transactions');		
		$result = $query->num_rows();
		return $result;		
	}

	function getAllRecords($searchData) {	
		$this->db->select('tbl_transactions.*');
		$this->db->select('tbl_members.first_name,tbl_members.last_name');		
		if(isset($searchData['keyword']) && $searchData['keyword']) {
			$this->db->group_start();					
				$this->db->like('tbl_transactions.amount',$searchData['keyword']);
				$this->db->or_like('tbl_members.first_name',$searchData['keyword']);
				$this->db->or_like('tbl_members.last_name',$searchData['keyword']);
			$this->db->group_end();
		}	
		if(isset($searchData['sorting_order']) && $searchData['sorting_order']) {
			$this->db->order_by('tbl_transactions.id', $searchData['sorting_order']);
		}	
		if(isset($searchData['date']) && $searchData['date']){			
			$this->db->like('tbl_transactions.add_date',$searchData['date']);
		}
		if(isset($searchData['limit']) && $searchData['limit'] )
		$this->db->limit($searchData['limit'],$searchData['search_index']);
		$this->db->join('tbl_members','tbl_members.id = tbl_transactions.user_id','LEFT');		
		$query = $this->db->get('tbl_transactions');
		//echo $this->db->last_query();die;
		$result = $query->result();
		return $result;
	}

	function getSingleRecordById($id) {
		$this->db->select('tbl_transactions.*');
		$this->db->select('tbl_members.first_name,tbl_members.last_name');		
		$this->db->join('tbl_members','tbl_members.id=tbl_transactions.user_id','left');	
		$this->db->where('tbl_transactions.id',$id);		
		$query = $this->db->get('tbl_transactions');
		$result = $query->row();
		return $result;
	}

	function deleteRecordById($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

}