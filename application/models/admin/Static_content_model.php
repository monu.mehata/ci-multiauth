<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Static_content_model extends CI_Model 
{
	function __construct() 
	{
		parent::__construct();
		$this->table = 'tbl_static_content';
	}
	 
	function getAllRecords(){
		$this->db->select('*');
		$this->db->order_by('id','DESC');
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}

	function getSingleRecordById($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		$result = $query->row();
		return $result;
	}

	function updateSingleRecordById($id,$data){
		$this->db->where('id',$id);
		$result = $this->db->update($this->table,$data);
		return $result;
	}

	function changeStatusById($id,$status){
		$this->db->where('id',$id);
		$this->db->update($this->table,array('status' => $status));
	}
}