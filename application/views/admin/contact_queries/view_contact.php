<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/contact_queries')?>" class="tooltips" data-original-title="List of contact us" data-placement="top" data-container="body"> List of contact us</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>Reply contact us</li>	
		<li style="float:right;">
			<a class="btn red tooltips" href="<?php echo base_url('admin/contact_queries'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
			</a>
		</li>				
	</ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="icon-globe"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>
			</div>		
			<div class="portlet-body form">		
				<div class="form-body">
					<?php if(!empty($contactQueries)) {?>
						<div class="portlet portlet-sortable box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span>Contact us details</span>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body portlet-empty"> 
                            	 <section style="margin-top: 20px; background-color: white; ">
							        <table class="table table-bordered table-condensed">												<tr>
							                <th>Sender name</th>
							                <td style="word-break: break-all;"><?php echo htmlentities($contactQueries->your_name); ?></td>
							            </tr>
							            <tr>
							                <th>Subject</th>
							                <td style="word-break: break-all;"><?php echo htmlentities($contactQueries->subject); ?></td>
							            </tr>
							            <tr>
							                <th>Email</th>
							                <td style="word-break: break-all;"><?php echo htmlentities($contactQueries->email); ?></td>
							            </tr>							            
							            <tr>
							                <th>Message</th>
							                <td style="word-break: break-all;"><?php echo $contactQueries->message; ?></td>
							            </tr>							            							            
							            <tr>
							                <th>Send date</th>							                
							                <td style="word-break: break-all;"><?php echo date('M d, Y',strtotime($this->common_model->getGMTDateToLocalDate($contactQueries->add_date)));?></td>
							            </tr>							          
                                        <?php if($contactQueries->reply_message){ ?>
								            <tr>
								                <th>My reply message</th>
								                <td style="word-break: break-all;"><?php echo $contactQueries->reply_message; ?></td>
								            </tr>	
								        <?php } ?>
								        <?php if($contactQueries->replied_date){ ?>
							            <tr>
							                <th>Date replied</th>							                
							                <td style="word-break: break-all;"><?php echo date('M d, Y, h:i:a',strtotime($this->common_model->getGMTDateToLocalDate($contactQueries->replied_date)));?></td>
							            </tr>
							            <?php } ?>
                                        <tr>
										    <th>Reply</th>
										    <td>
										        <div class="btn-group">
													<?php if($contactQueries->is_replied == 'Yes') { ?>
														<button class="btn btn-xs btn-success status_label" type="button">Yes</button>
													<?php } else { ?>
														<button class="btn btn-xs btn-danger status_label" type="button">No</button>
													<?php } ?>
												</div>
											</td>
										</tr>
																	           							           							          							          							          							           
							          </table>
							    </section>								                       
							</div>
                        </div>						 
					    <?php } else {?>
					    	<div class="alert alert-info">No record found</div>
					    <?php } ?>
				</div>

				<?php if($contactQueries->is_replied=='No'){ ?>
					<div class="form-body">
						<div class="portlet portlet-sortable box green-haze">
	                        <div class="portlet-title">
	                            <div class="caption">
	                                <span>Reply</span>
	                            </div>
	                            <div class="tools">
	                                <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
	                            </div>
	                        </div>
	                        <div class="portlet-body portlet-empty"> 
	                        	 <section style="margin-top: 20px; background-color: white; ">
							        <?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
							        <input type="hidden" name="date_query" value="<?php echo date('M d, Y',strtotime($this->common_model->getGMTDateToLocalDate($contactQueries->add_date)));?>">

			                    	
									<div class="portlet-body portlet-empty">  
			                        	<div class="form-group form-md-line-input">
											<label for="form_control_tags" class="control-label col-md-2">Message reply <span style="color:red">*</span></label>
											<div class="col-md-10">								
												<?php echo form_textarea(array('placeholder' => "Enter message reply", 'id' => "reply_message",'rows' =>'4', 'name' => "reply_message", 'class' => "form-control", 'value' => "")); ?>
												<div class="form-control-focus"> </div>
											</div>
										</div>						
									</div>

									<div class="form-actions noborder">
										<div class="row">
											<div class="col-md-offset-2 col-md-10">
											<button type="submit" class="btn green">Submit</button>
											<a href="<?php echo base_url('admin/contact_queries'); ?>" class="btn default">Cancel</a>
										</div>
										</div>
									</div>
							        </form>
							    </section>								                       
							</div>
	                    </div>						 
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>