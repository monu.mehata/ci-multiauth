<div class="row margin-top-10">
   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat2">
			<div class="display">
				<div class="number">
					<h3 class="font-purple-soft"><?php echo $membersCount; ?></h3>
					<a class="hd_title" href="<?php echo site_url('admin/members');?>">Total registered users</a>
				</div>
				<div class="icon">
					<i class="fa fa-user-plus" aria-hidden="true"></i>
				</div>
			</div>
			<div class="progress-info">
				<div class="progress">
					<span style="width: <?php echo ($membersCount<100)?$membersCount:100; ?>%;" class="progress-bar progress-bar-success purple-soft">
					<span class="sr-only"><?php echo $membersCount; ?>% change</span>
					</span>
				</div>
			</div>
		</div>
	</div>
	

</div>
