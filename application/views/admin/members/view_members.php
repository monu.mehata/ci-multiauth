
<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/members')?>" class="tooltips" data-original-title="List of members" data-placement="top" data-container="body">List of members</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>View member</li>	
		<li style="float:right;">
			<a class="btn red tooltips" href="<?php echo base_url('admin/members'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
			</a>
		</li>				
	</ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="icon-globe"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>
			</div>		
			<div class="portlet-body form">		
					<div class="form-body">
						<?php if(!empty($alluserview)) {?>
							<div class="portlet portlet-sortable box green-haze">
	                            <div class="portlet-title">
	                                <div class="caption">
	                                    <span>Members details</span>
	                                </div>
	                                <div class="tools">
	                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
	                                </div>
	                            </div>
	                            <div class="portlet-body portlet-empty"> 
	                            	<section style="margin-top: 20px; background-color: white; ">
								        <table class="table table-bordered table-condensed">		     
								            
								             <tr>
								                <th>Name</th>
								                <td style="word-break: break-all;text-align: justify;"><?php echo htmlentities($alluserview->first_name.' '.$alluserview->last_name); ?></td>
								            </tr>
								            <tr>
								            	<th>Username</th>
								            	<td><?php echo htmlentities($alluserview->username); ?></td>
								            </tr>
								            <tr>
								            	<th>Email</th>
								            	<td><?php echo htmlentities($alluserview->email); ?></td>
								            </tr>	
								            <tr>
								            	<th>Mobile number</th>
								            	<td><?php echo htmlentities($alluserview->phone); ?></td>
								            </tr>		            	
                                            <tr>
								                <th>Add date</th>
								                <td><?php echo date('M d, Y',strtotime($this->common_model->getGMTDateToLocalDate($alluserview->add_date))); ?></td>
								            </tr>								           
								            <tr>
								                <th>Status</th>
								                <td>
									                <div class="btn-group">
														<?php if($alluserview->status == 'Active') { ?>
															<button class="btn btn-xs btn-success status_label" type="button">Active</button>
														<?php } else { ?>
															<button class="btn btn-xs btn-danger status_label" type="button">Inactive</button>
														<?php } ?>
													</div>
												</td>
								            </tr>									             							           
								        </table>
								    </section>								                       
								</div>
	                        </div>						 
						    <?php } else {?>
						    	<div class="alert alert-info">No record found</div>
						    <?php } ?>
					</div>
			</div>
		</div>
	</div>
</div>