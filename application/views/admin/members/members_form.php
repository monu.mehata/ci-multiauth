<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<a href="<?php echo site_url('admin/members')?>" class="tooltips" data-original-title="List of members" data-placement="top" data-container="body">List of members</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<?php if($id != '')
				 	{?>Update member<?php }
				  else 
				  	{?>Add member<?php }
				  ?>			
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/members'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<div class="portlet-body form">
				<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
					<div class="form-body">   
				
						<div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">First name<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_input(array('placeholder' => "Enter first name", 'id' => "first_name", 'name' => "first_name", 'class' => "form-control", 'value' => "$first_name")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div>  

            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Last name<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_input(array('placeholder' => "Enter last name", 'id' => "last_name", 'name' => "last_name", 'class' => "form-control", 'value' => "$last_name")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div>

            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Email<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_input(array('placeholder' => "Enter email", 'id' => "email", 'name' => "email", 'class' => "form-control", 'value' => "$email")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div>  

            <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Username<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_input(array('placeholder' => "Enter username", 'id' => "username", 'name' => "username", 'class' => "form-control", 'value' => "$username")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div>

             <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Mobile number<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_input(array('placeholder' => "Enter mobile number", 'id' => "phone", 'name' => "phone", 'class' => "form-control", 'value' => "$phone")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div> 

             <div class="form-group form-md-line-input">
              <label for="form_control_title" class="control-label col-md-2">Password<span style="color:red">*</span></label>
              <div class="col-md-10">         
                <?php echo form_password(array('placeholder' => "Enter password", 'id' => "password", 'name' => "password", 'class' => "form-control", 'value' => "$password")); ?>
                <div class="form-control-focus"> </div>     
              </div>
            </div> 
																																										
						<div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Status</label>
							<div class="col-md-10">
								<div class="md-radio-inline">
								<div class="md-radio">
								<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
								<label for="radio19">
								<span class="inc"></span>
								<span class="check"></span>
								<span class="box"></span>Active</label>
								</div>
								<div class="md-radio has-error">
								<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
								<label for="radio20">
								<span class="inc"></span>
								<span class="check"></span>
								<span class="box"></span>Inactive</label>
								</div>
								</div>
							</div>
						</div> 					
						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
								<button type="submit" class="btn green">Submit</button>
								<a href="<?php echo base_url('admin/members'); ?>" class="btn default">Cancel</a>
							</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
var element = 'content';
CKEDITOR.replace(element,
{
	   
});	
</script>

<script type="text/javascript">
	$('.ajax_form').submit(function(event){
		for (instance in CKEDITOR.instances)
	    {
	        CKEDITOR.instances[instance].updateElement();
	    }
	});
</script>

<script>
$(document).ready(function () {
	$('#birth_date').datepicker(
		{
			format: 'yyyy-mm-dd',			
			autoclose:true
		}
	);
});

function showReferred($value){
	if($value=='Yes'){
       $('#referred_div').show();
	}
	else {
       $('#referred_div').hide();
	}
}

$(document).ready(function(){
    var other_reference = $('#other_reference').val();
    showReferred(other_reference);
});
</script>

<script type="text/javascript">
var clinic_id = '<?php echo isset($clinic_id)?$clinic_id:'' ?>';
var doctor_id = '<?php echo isset($doctor_id)?$doctor_id:'' ?>';

$(document).ready(function(){
    getDoctorsFromClinic(clinic_id); 
});

function getDoctorsFromClinic(clinic_id){  	
  if(clinic_id) {  
        $.getJSON(siteurl+'admin/doctors/getAllDoctorsByClinicId?clinic_id='+clinic_id,function(data){
          $(".doctors_data").html('');
          $(".doctors_data").select2("val", "");
          if(data.doctorsRecord) {
            var html = '<option value="">Select doctor</option>';
            $(".doctors_data").append(html);
            $.each(data.doctorsRecord,function(key,value){   
              var selected = '';
              if(doctor_id == value.userId)
              {
                selected = 'selected';
              }                                     
              var html = '<option value="'+value.userId+'" '+selected+'>'+value.full_name+'</option>';
              $(".doctors_data").append(html);
            }); 
          }
          else {
            var html = '<option value="">No record found</option>';
            $(".doctors_data").append(html);
          }
        });
      }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCxhgcC9Un6YMIVL5agYr7ygNvQMt306Nc&sensor=false&libraries=places"></script>
<script type="text/javascript">
var source_lattitude = '';
var source_longitude = '';


/*var address_input = document.getElementById('location');
var address_autocomplete = new google.maps.places.Autocomplete(address_input);*/

/* code for load default map */
function addresslatLong(lat,long){
  window.source_lattitude = lat;
  window.source_longitude = long;
}

var address_input = document.getElementById('location').value;
var geocoder =  new google.maps.Geocoder();
geocoder.geocode( { 'address': address_input}, function(results, status) {
  if(status == google.maps.GeocoderStatus.OK) {
    source_lattitude = results[0].geometry.location.lat();
    source_longitude = results[0].geometry.location.lng();
    addresslatLong(source_lattitude,source_longitude);
  }
});
/* code for load default map */


var geocoder = new google.maps.Geocoder();
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress();
    }
  });
}

function updateMarkerStatus(str) {
  //document.getElementById('markerStatus').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  source_lattitude = latLng.lat();
  source_longitude = latLng.lng();
  $('#latitude').val(latLng.lat());
  $('#longitude').val(latLng.lng());
}
function updateMarkerAddress(str) 
{ 
  $('#location').val(str); 
}
function initialize() {
    var address_latLng = new google.maps.LatLng(source_lattitude,source_longitude);
    var mapOptions = {
      center: address_latLng,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map-0'),
      mapOptions);
    var address_input = document.getElementById('location');
    var address_autocomplete = new google.maps.places.Autocomplete(address_input);
    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      map: map,
      position: address_latLng,
      title: 'Drag to change',
      map: map,
      draggable: true
    });
  updateMarkerPosition(address_latLng);
  //geocodePosition(address_latLng);



  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerStatus('Dragging...');
    updateMarkerPosition(marker.getPosition());
  });
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerStatus('Drag ended');
    geocodePosition(marker.getPosition());
    
  });

  google.maps.event.addListener(address_autocomplete, 'place_changed', function() {
    infowindow.close();
    var place = address_autocomplete.getPlace();
    if (place.geometry.viewport) 
    {
      map.fitBounds(place.geometry.viewport);
    } 
    else 
    {
      map.setCenter(place.geometry.location);
      map.setZoom(10);  // Why 17? Because it looks good.
    }
        
    marker.setPosition(place.geometry.location);
    updateMarkerPosition(place.geometry.location);
    var address = '';
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
      google.maps.event.addDomListener(radioButton, 'click', function() {
      autocomplete.setTypes(types);
    });
  }
}
google.maps.event.addDomListener(window, 'load', initialize);

</script>