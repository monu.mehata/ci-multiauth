<!DOCTYPE html>

<html lang="en" class="no-js">
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<title><?php echo getSiteOption('site_title',true); ?></title>
	<!-- <link rel="icon" href="<?php echo $this->config->item('admin_assets'); ?>global/img/sport_fav.png" sizes="192x192" /> -->
	<?php $this->load->view('admin/includes/common'); ?>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="<?php echo $this->config->item('admin_assets'); ?>admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/css/jquery.tagit.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>global/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>

	<script src="<?php echo $this->config->item('admin_assets'); ?>global/scripts/tag-it.js" type="text/javascript"></script>
</head>

<body class="page-md page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo">

<!-- BEGIN HEADER -->
<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<div class="page-header-inner">
		<div class="page-logo">
			<a href="<?php echo site_url('admin'); ?>">
			<img src="http://dummyimage.com/120x50/080208/e00000&text=LOGO+HERE" alt="logo" class="logo-default"/>
			<!-- <img src="<?php echo $this->config->item('admin_assets'); ?>global/img/sport_logo.png" alt="logo" class="logo-default"/> -->
			</a>
			<div class="menu-toggler sidebar-toggler"></div>
		</div>
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>

		<div class="page-top">
			<div class="top-menu">

				<ul class="nav navbar-nav pull-right">
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
						<?php echo $this->session->userdata('username') ?></span>
						<!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
						<img alt="" class="img-circle" src="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/img/avatar9.jpg"/>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="extra_profile.html">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<!-- <li class="divider"> -->
							<li id="website_header">
								<a target="_blank" href="<?php echo site_url('') ?>">
								<i class="fa fa-globe" aria-hidden="true"></i> Website
								</a>
							</li>
							<!-- <li class="divider"> -->
							</li>
							<li id="admin_change_password">
								<a href="<?php echo site_url('admin/change-password') ?>">
								<i class="icon-lock"></i>Change Password
								</a>
							</li>
							<!-- <li class="divider"> -->
							</li>
							<li id="admin_update_profile">
								<a href="<?php echo site_url('admin/update-profile') ?>">
								<i class="fa fa-user"></i>Update Profile
								</a>
							</li>
							<!-- <li class="divider"> -->
							</li>
							<li id="admin_log_out">
								<a href="<?php echo site_url('admin/logout') ?>">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					
				</ul>
			</div>
			
		</div>
		
	</div>
	
</div>

<div class="clearfix">
</div>
<div class="page-container">
	<?php $this->load->view('admin/includes/left_menu'); ?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1><?php echo $page_title; ?></h1>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="alert_area">
					</div>
				</div>
			</div>
			