			</div>
		</div>

		<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner">
				 <?php echo getSiteOption('copyright_text',true); ?>	
			</div>
			<div class="scroll-to-top">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- END FOOTER -->

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo $this->config->item('admin_assets'); ?>global/scripts/metronic.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/scripts/layout.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('admin_assets'); ?>admin/layout2/scripts/quick-sidebar.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('admin_assets'); ?>admin/layout4/scripts/demo.js" type="text/javascript"></script>
		<script src="<?php echo $this->config->item('admin_assets'); ?>admin/pages/scripts/tasks.js" type="text/javascript"></script>

		<!-- END PAGE LEVEL SCRIPTS -->
		<script>
		jQuery(document).ready(function() {    
		   Metronic.init(); 
		   Layout.init(); 
		   Demo.init(); 
		   QuickSidebar.init();
		});
		</script>
	</body>
</html>