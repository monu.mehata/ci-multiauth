	<div class="page-sidebar-wrapper">
		<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<li class="start <?php echo ($left_menu == 'Dashboard')?'active':''; ?>" id="admin_dashboard">
					<a href="<?php echo site_url('admin'); ?>">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
				</li>
				<li class="<?php echo ($left_menu == 'Members_module')?'active open':''; ?>" id="members_module">
					<a href="javascript:;">
						<i class="fa fa-male" aria-hidden="true"></i>
						<span class="title">Members module</span>
						<span class="arrow <?php echo ($left_menu == 'members_module')?'open':''; ?>"></span>
					</a>
					<ul class="sub-menu">
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Members')?'active':''; ?>" id="members">
							<a href="<?php echo site_url('admin/members'); ?>">
								<i class="fa fa-address-book" aria-hidden="true"></i>
								<span class="title">Members</span>	
							</a>
						</li>												
					</ul>
				</li>
			
				
				<li class="<?php echo ($left_menu == 'Transactions_module')?'active open':''; ?>" id="transactions">
					<a href="javascript:;">
					<i class="fa fa-cc-discover" aria-hidden="true"></i>
					<span class="title">Transactions module</span>
					<span class="arrow <?php echo ($left_menu == 'Transactions_module')?'open':''; ?>"></span>
					</a>
					<ul class="sub-menu">
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Transactions')?'active':''; ?>" id="transactions">
							<a href="<?php echo site_url('admin/transactions'); ?>">
							<i class="fa fa-cc-mastercard" aria-hidden="true"></i>
							<span class="title">Transactions</span>	
							</a>
						</li>												
					</ul>
				</li>
				
				<li class="<?php echo ($left_menu == 'Site_options')?'active open':''; ?>" id="admin_site_options">
					<a href="javascript:;">
					<i class="fa fa-pencil-square-o"></i>
					<span class="title">Site options module</span>
					<span class="arrow <?php echo ($left_menu == 'Site_options')?'open':''; ?>"></span>
					</a>
					<ul class="sub-menu">
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Website_setting')?'active':''; ?>" id="website">
							<a href="<?php echo base_url('admin/website'); ?>">		
							<i class="icon-settings"></i>
							<span class="title">Website setting</span>	
							</a>
						</li>
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Mail_templates')?'active':''; ?>" id="email_templates">
							<a href="<?php echo site_url('admin/mail_templates'); ?>">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<span class="title">Email templates</span>	
							</a>
						</li>	
						
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'static_page')?'active':''; ?>" id="static_page">
							<a href="<?php echo site_url('admin/static_page'); ?>">
							<i class="fa fa-file" aria-hidden="true"></i>
							<span class="title">Static pages</span>	
							</a>
						</li>
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'static_content')?'active':''; ?>" id="static_content">
							<a href="<?php echo site_url('admin/static_content'); ?>">
							<i class="fa fa-paragraph" aria-hidden="true"></i>
							<span class="title">Static contents</span>	
							</a>
						</li>
						<li class="<?php echo (isset($left_submenu) && $left_submenu == 'Contact')?'active':''; ?>" id="contact_queries">
							<a href="<?php echo site_url('admin/contact_queries'); ?>">
							<i class="fa fa-phone-square" aria-hidden="true"></i>
							<span class="title">Contact us</span>
							</a>
						</li>	 														
					</ul>
				</li> 
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR