<?php
	if (isset($message) && $message) {
		$alert = ($success)? 'alert-success':'alert-danger';
		echo '<div class="alert ' . $alert . '">' . $message . '</div>';
	}
?>
<div class="portlet light" style="height:45px">
	<div class="row">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<a href="<?php echo site_url('admin/static_content')?>" class="tooltips" data-original-title="List of static contents" data-placement="top" data-container="body">List of static contents</a>
				<i class="fa fa-arrow-right"></i>
			</li>
			<li>
				<?php if($id != '')
				 	{?>Update static content <?php }
				  else 
				  	{?>Add static content <?php }
				  ?>			
			</li>	
			<li style="float:right;">
				<a class="btn red tooltips" href="<?php echo base_url('admin/static_content'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
				</a>
			</li>				
		</ul>			
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="fa fa-file-image"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>				
			</div>
			<div class="portlet-body form">
				<?php echo form_open(current_url(), array('class' => 'form-horizontal ajax_form'));?>
					<div class="form-body">								
					    <div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Title<span style="color:red">*</span></label>
							<div class="col-md-10">					
								<?php echo form_input(array('placeholder' => "Enter title", 'id' => "title", 'name' => "title", 'class' => "form-control", 'value' => "$title")); ?>
								<div class="form-control-focus"> </div>			
							</div>
						</div>
														    				 						
                    	<div class="form-group form-md-line-input">
							<label for="email_content" class="control-label col-md-2">Content<span style="color:red">*</span></label>
							<div class="col-md-10">		
							<?php echo form_textarea(array('placeholder' => 'Enter content', 'id' => "content", 'name' => "content", 'class' => " form-control", 'value' => "$content")); ?>
							</div>
						</div>														

						<div class="form-group form-md-line-input">
							<label for="form_control_image" class="control-label col-md-2">Image<span style="color:red">*</span></label>
							<div class="col-md-6">
								<input type="file" name="image_name" id="image_name" style="width: 250px;">
								
							</div>
							<div class="col-md-2">
							<?php
								if ($image_name) {
									echo '<img src="' . base_url('./assets/uploads/static_content/'. $image_name) .'" widht="100" height="100">';
								}
							?>
							</div>
						</div> 
						<div class="form-group form-md-line-input">
							<label for="form_control_title" class="control-label col-md-2">Status</label>
							<div class="col-md-10">	
								<div class="md-radio-inline">
									<div class="md-radio">
										<input id="radio19" class="md-radiobtn" type="radio" name="status" value="Active" <?php echo ($status == 'Active')?'checked':''; ?>>
										<label for="radio19">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Active</label>
									</div>
									<div class="md-radio has-error">
										<input id="radio20" class="md-radiobtn" type="radio" name="status" value="Inactive" <?php echo ($status == 'Inactive')?'checked':''; ?>>
										<label for="radio20">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>Inactive</label>
									</div>
								</div>
							</div>
						</div>	

						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-offset-2 col-md-10">
									<button type="submit" class="btn green">Submit</button>
									<a href="<?php echo base_url('admin/static_content'); ?>" class="btn default">Cancel</a>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>



<script type="text/javascript">
var element = 'content';
CKEDITOR.replace(element,
{
	// filebrowserBrowseUrl: '<?php echo base_url() ?>'+'assets/admin/assets/global/plugins/ckfinder/ckfinder.html',
 //    filebrowserUploadUrl: '<?php echo base_url() ?>'+'assets/admin/assets/global/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',		   
});	
</script>

<script type="text/javascript">
$('.ajax_form').submit(function(event){
	for (instance in CKEDITOR.instances)
    {
        CKEDITOR.instances[instance].updateElement();
    }
});
</script>