
<table class="table table-striped table-bordered table-hover resumes_table" id="sample_3">
	<thead>
		<tr>	
			<th>Name</th>            					
			<th>Amount</th>								
			<th>Payment date</th>
			<th>Option</th>
		</tr>
	</thead>
	<tbody>
		<?php if($alltransactiondetails) {?>
			<?php foreach ($alltransactiondetails as $key => $value) { ?>				
				<tr class="odd gradeX">							
					<td><?php echo showLimitedText(htmlentities($value->first_name.' '.$value->last_name),25);?></td> 
					<td>$<?php echo showLimitedText(htmlentities($value->amount),25);?></td>		
                    <td><?php echo date('M d, Y',strtotime($this->common_model->getGMTDateToLocalDate($value->add_date)));?></td>
					<td class="text-center">									
						<a href="<?php echo base_url('admin/transactions/view/'.$value->id) ?>" class="btn yellow tooltips" data-original-title="View this record" data-placement="top" data-container="body"><i class="fa fa-eye"></i></a>
						<a data-toggle="modal" data-id="<?php echo $value->id; ?>" data-url="<?php echo base_url('admin/transactions/delete'); ?>" class="btn btn-danger tooltips" onClick="deleteRecord(this);" data-original-title="Delete this record" data-placement="top" data-container="body"><i class="fa fa-remove"></i></a>									
					</td>
				</tr>
			<?php } ?>
		<?php } else { ?>	
			<tr>
			<td colspan="8"><div class="no-record">Sorry No Record found</div></td>
			</tr>
		<?php } ?>
	</tbody>	
</table>
<script type="text/javascript">
	$(function(){
		$("input:checkbox").uniform();
	})
</script>
