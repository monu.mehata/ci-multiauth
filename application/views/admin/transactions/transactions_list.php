<?php
    if ($this->session->flashdata('message')) {
        echo '<div class="alert alert-success">' . $this->session->flashdata('message') . '</div>';
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="box grey-cascade">          
            <div class="portlet-body">
                
            </div>
        </div>
        <div class="row">
                <div class="col-md-12">                   
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-docs"></i>List of transactions
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-12">

                                    </div>                      
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-1 col-sm-12">
                                        <div class="form-group">
                                            <select name="page_limit" class="form-control input-xsmall input-inline" id="page_limit" onchange="searchRecords(this)">    
                                                <option value="5" >5</option>
                                                <option value="15" selected>15</option>
                                                <option value="20">20</option>                                               
                                                <option value="100">100</option>
                                                <option value="">All</option>           
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <select name="sorting_order" class="form-control" id="sorting_order" onchange="searchRecords(this)">
                                                <option value="ASC">Ascending</option>
                                                <option value="DESC" selected>Descending</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 pull-right">
                                        <div class="">
                                            <input class="form-control" placeholder="Search by keyword" type="text" id="keyword" name="keyword" value="" onkeyup="searchRecords(this)">                                                             
                                        </div>  
                                    </div> 

                                    <div class="col-md-3 col-sm-12 pull-right">
                                        <div class="">
                                            <input class="form-control" placeholder="Search by date" type="text" id="date" name="date" value="" onchange="searchRecords();" readonly="">                                                             
                                        </div>  
                                        <a class="clear_search" style="display: none;" href="<?php echo base_url('admin/transactions');?>">Reset</a>
                                    </div>  
                                </div>
                                
                            </div>
                            <div class="ajax_content">


                            </div>      
                            <div class="row">                               
                                <div class="col-md-12"> 
                                    <div class="paging_div pull-right">
                                    </div>  
                                </div>
                            </div>                                                      
                        </div>
                    </div>
                </div>
            </div>      
    </div>
</div>
<script type="text/javascript">
function searchRecords()
{    
    var date = $('#date').val();    
    var keyword = $('#keyword').val();
    var page_limit = $('#page_limit').val();    
    var sorting_order = $('#sorting_order').val();   
    var surl = siteurl+'admin/transactions/get_ajax_list?keyword='+keyword+'&page_limit='+page_limit+'&sorting_order='+sorting_order+'&date='+date;
    getAjaxSearchData(surl);
}

function getAjaxSearchData(surl)
{
    $.getJSON(surl,function(response){
        if (response.success) 
        {
            $('.ajax_content').html(response.html);
            $('.paging_div').html(response.paging); 
            if(response.clear_search_div){                      
                $(".clear_search").css("display", "block");
            }         
        }
    });
}

$(document).ready(function(){
  searchRecords();
  $(document).on('click','.pagination a',function(e){
    e.preventDefault();
    if($(this).attr('href'))
    {
      getAjaxSearchData($(this).attr('href'));
    }
    return false;
  })
});
</script>

<script>
$(document).ready(function () {
    
$('#date').datepicker(
{
format: 'yyyy-mm-dd',
// startDate:'now', 
autoclose:true
}
);
});

</script>