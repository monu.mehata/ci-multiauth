<div class="portlet light" style="height:45px">
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo site_url('admin')?>" class="tooltips" data-original-title="Home" data-placement="top" data-container="body">Home</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>			
			<a href="<?php echo site_url('admin/transactions')?>" class="tooltips" data-original-title="List of transactions" data-placement="top" data-container="body">List of transactions</a>
			<i class="fa fa-arrow-right"></i>
		</li>
		<li>View transaction</li>	
		<li style="float:right;">
			<a class="btn red tooltips" href="<?php echo base_url('admin/transactions'); ?>" style="float:right;margin-right:3px;margin-top: -7px;" data-original-title="Go Back" data-placement="top" data-container="body">Go Back<i class="m-icon-swapleft m-icon-white"></i>
			</a>
		</li>				
	</ul>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="row">
					<div class="col-md-6">
						<div class="caption font-red-sunglo">
							<i class="icon-globe"></i>
							<span class="caption-subject bold uppercase"><?php echo $page_title; ?></span>
						</div>
					</div>					
				</div>
			</div>		
			<div class="portlet-body form">		
				<div class="form-body">
					<?php if(!empty($transactions)) {?>
						<div class="portlet portlet-sortable box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span>Transaction details</span>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                                </div>
                            </div>
                            <div class="portlet-body portlet-empty"> 
                            	 <section style="margin-top: 20px; background-color: white; ">
							        <table class="table table-bordered table-condensed">	
							            <tr>
								            <th>Name</th>
								            <td style="word-break: break-all;"><?php echo $transactions->first_name." ".$transactions->last_name ;?></td>
								        </tr>
							            <tr>
							                <th>Amount</th>
							                <td style="word-break: break-all;"><?php echo $transactions->amount; ?></td>
							            </tr>
							            <tr>
							                <th>Payment status</th>
							                <td style="word-break: break-all;"><?php echo $transactions->payment_status; ?></td>
							            </tr>          				
							            <tr>
							                <th>Add date</th>
							                <td><?php echo date('M d, Y',strtotime($this->common_model->getGMTDateToLocalDate($transactions->add_date))); ?></td>
								        </tr>						           							           
							          </table>
							    </section>								                       
							</div>
                        </div>						 
					    <?php } else {?>
					    	<div class="alert alert-info">No record found</div>
					    <?php } ?>
				</div>				
			</div>
		</div>
	</div>
</div>