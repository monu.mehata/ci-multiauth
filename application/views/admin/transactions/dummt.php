<?php
function index()
	{
		$output['page_title'] = 'Transactions module';
		$output['left_menu'] = 'Transactions_options';
		$output['left_submenu'] = 'transactions';
    	$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/transactions/transactions_list');
		$this->load->view('admin/includes/footer');
	}

	function get_ajax_list() {		
		$this->load->library('pagination');	
		$keyword = $this->input->get('keyword');
        $date = $this->input->get('date');
		$sorting_order = $this->input->get('sorting_order');	
		$page_limit = $this->input->get('page_limit');	
		$page_no = $this->input->get('page_no')?$this->input->get('page_no'):1;
		$page_no_index = ($page_no - 1) * $page_limit;
		$sQuery = '';
		if($keyword)
			$sQuery = $sQuery.'&keyword='.$keyword;	
		if($sorting_order)
			$sQuery = $sQuery.'&sorting_order='.$sorting_order;	
		if($page_limit)
			$sQuery = $sQuery.'&page_limit='.$page_limit;	
        $per_page = $this->input->get('page_limit');
		$searchData['search_index'] = $page_no_index;
		$searchData['limit'] = $per_page;		
		$searchData['keyword'] = $keyword;	
		$searchData['sorting_order'] = $sorting_order; 
		$searchData['date'] = $date; 
	   	$config['base_url'] = site_url('admin/transactions/get_ajax_list?'.$sQuery);
		$total_rows = $this->transactions->countAllRecords($searchData);		
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();
		$records = $this->transactions->getAllRecords($searchData);	
		$output['records'] = $records;		
		$totalAmount = 0;
		foreach ($records as $key => $value) {
			$totalAmount += $value->amount;
		}
		$output['totalRecords'] = $total_rows;	
		$output['totalAmount'] = $totalAmount;	
		$html = $this->load->view('admin/transactions/ajax_list',$output,true);
		if($date){
			$data['clear_search_div']  = true;;
		}
		$data['success'] = true;
		$data['html'] = $html;	
		$data['paging']	= $paging; 
		echo json_encode($data); die;	
	}



	function countAllRecords($searchData) {	
	    $this->db->select('tbl_transactions.*');
		$this->db->select('tbl_users.first_name,tbl_users.last_name');	
		if(isset($searchData['keyword']) && $searchData['keyword']) {
			$this->db->group_start();
				$this->db->like('tbl_users.full_name',$searchData['keyword']);				
				$this->db->or_like('tbl_transactions.payer_email',$searchData['keyword']);
				$this->db->or_like('tbl_transactions.amount',$searchData['keyword']);
			$this->db->group_end();
		}	
		if(isset($searchData['sorting_order']) && $searchData['sorting_order']) {
			$this->db->order_by('tbl_transactions.id', $searchData['sorting_order']);
		}
		if(isset($searchData['date']) && $searchData['date']){			
			$this->db->like('tbl_transactions.add_date',$searchData['date']);
		}			
		$this->db->join('tbl_users','tbl_users.id = tbl_transactions.user_id','LEFT');
		$query = $this->db->get('tbl_transactions');		
		$result = $query->num_rows();
		return $result;		
	}

	function getAllRecords($searchData) {	
		$this->db->select('tbl_transactions.*');
		$this->db->select('tbl_users.first_name,tbl_users.last_name');		
		if(isset($searchData['keyword']) && $searchData['keyword']) {
			$this->db->group_start();
				$this->db->like('tbl_users.full_name',$searchData['keyword']);						
				$this->db->or_like('tbl_transactions.payer_email',$searchData['keyword']);
				$this->db->or_like('tbl_transactions.amount',$searchData['keyword']);
			$this->db->group_end();
		}	
		if(isset($searchData['sorting_order']) && $searchData['sorting_order']) {
			$this->db->order_by('tbl_transactions.id', $searchData['sorting_order']);
		}	
		if(isset($searchData['date']) && $searchData['date']){			
			$this->db->like('tbl_transactions.add_date',$searchData['date']);
		}
		if(isset($searchData['limit']) && $searchData['limit'] )
		$this->db->limit($searchData['limit'],$searchData['search_index']);
		$this->db->join('tbl_users','tbl_users.id = tbl_transactions.user_id','LEFT');		
		$query = $this->db->get('tbl_transactions');
		$result = $query->result();
		return $result;
	}

	<script>
function searchRecords()
{    
    var date = $('#date').val();    
    var keyword = $('#keyword').val();
    var page_limit = $('#page_limit').val();    
    var sorting_order = $('#sorting_order').val();   
    var surl = siteurl+'admin/transactions/get_ajax_list?keyword='+keyword+'&page_limit='+page_limit+'&sorting_order='+sorting_order+'&date='+date;
    getAjaxSearchData(surl);
}

function getAjaxSearchData(surl)
{
    $.getJSON(surl,function(response){
        if (response.success) 
        {
            $('.ajax_content').html(response.html);
            $('.paging_div').html(response.paging); 
            if(response.clear_search_div){                      
                $(".clear_search").css("display", "block");
            }         
        }
    });
}

	</script>