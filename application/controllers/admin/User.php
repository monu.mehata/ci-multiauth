<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->common_model->checkLoginAdminStatus();
		$this->load->model('admin/User_model', 'users');
		$this->load->model('mailsending_model', 'mailsend');
	}	

	function updateProfile() 
	{
		$output['page_title'] = 'Update profile';
		$output['left_menu'] = 'Members';
		$output['message'] ='';
		$userId = $this->session->userdata('admin_id');
		$user = $this->users->getUser($userId);
		if(isset($_POST) && !empty($_POST))
		{			
			$this->form_validation->set_rules('username',   'User name', 'trim|required|min_length[3]|max_length[15]|regex_match[/^[-A-Za-z0-9_ ]+$/]');
			$this->form_validation->set_rules('email',     'Email address', 'trim|required|valid_email');
			if ($this->form_validation->run()) 
			{
				if(!$this->users->checkEmailAddressUnique($this->input->post('email'),$userId))
				{
					if(!$this->users->checkUsernameUnique($this->input->post('username'),$userId))
					{
						$input = array();						
						$input['username'] = strtolower($this->input->post('username'));
						$input['email'] = $this->input->post('email');
						$this->users->updateUser($userId, $input);
						$this->session->set_userdata(array('username' => $this->input->post('username')));
						$message = 'Profile updated successfully';
						$success = true;	
						$output['redirectURL'] = site_url('admin/update-profile');					
					}
					else
					{
						$message = 'Username field must contain a unique value.';
						$success = false;
					}					
				}
				else
				{
					$message = 'The Email field must contain a unique value.';
					$success = false;
				}
				
			}
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}
		$output['first_name'] = $user->first_name;
		$output['last_name'] = $user->last_name;
		$output['username'] = $user->username;
		$output['email'] = $user->email;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/user/update_profile');
		$this->load->view('admin/includes/footer');
	}

   
	function changePasswordForAdmin() {
		$output['page_title'] = 'Change password';
		$output['left_menu'] = 'Members';
		$output['message'] ='';
		if(isset($_POST) && !empty($_POST))
		{
			$this->form_validation->set_rules('current_password', 'Current password', 'trim|required');
			$this->form_validation->set_rules('new_password', 'New password', 'trim|required|matches[re_new_password]|min_length[6]|max_length[15]');
			$this->form_validation->set_rules('re_new_password', 'Confirmation password', 'trim|required|min_length[6]|max_length[15]');
			if ($this->form_validation->run()) 
			{
				$password = $this->input->post('current_password');
				$salt = 'Ijxo1A16';
				$ency_password = md5(md5($password).md5($salt));
				$userId = $this->session->userdata('admin_id');
				$user = $this->users->checkUserExistByOldPassword($ency_password,$userId);
				if($user)
				{
					$newPassword = $this->input->post('new_password');
					$salt = 'Ijxo1A16';
					$encyPassword = md5(md5($newPassword).md5($salt));
					$input = array();
					$input['password'] = $encyPassword;
					$this->users->updateUser($userId, $input);
					$message = 'Password changed successfully';
					$success = true;
					$output['resetForm'] = true;
					//$output['redirectURL'] = site_url('admin');
				}
				else
				{
					$message = 'Current password incorrect';
					$success = false;
				}
			}
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/user/change_password');
		$this->load->view('admin/includes/footer');
	}	
}
