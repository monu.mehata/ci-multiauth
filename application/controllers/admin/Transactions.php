<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Transactions extends CI_Controller 
{
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->load->model('admin/transactions_model', 'transactions');		
		$this->load->model('admin/members_model', 'members');		
	}

	function index()
	{
		$output['page_title'] = 'Transactions module';
		$output['left_menu'] = 'Transactions_module';
		$output['left_submenu'] = 'Transactions';
    	$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/transactions/transactions_list');
		$this->load->view('admin/includes/footer');
	}

	function get_ajax_list() 
	{		
		$this->load->library('pagination');	
		$keyword = $this->input->get('keyword');
        $date = $this->input->get('date');
		$sorting_order = $this->input->get('sorting_order');	
		$page_limit = $this->input->get('page_limit');	
		$page_no = $this->input->get('page_no')?$this->input->get('page_no'):1;
		$page_no_index = ($page_no - 1) * $page_limit;
		$sQuery = '';
		if($keyword)
			$sQuery = $sQuery.'&keyword='.$keyword;	
		if($sorting_order)
			$sQuery = $sQuery.'&sorting_order='.$sorting_order;	
		if($page_limit)
			$sQuery = $sQuery.'&page_limit='.$page_limit;	
        $per_page = $this->input->get('page_limit');
		$searchData['search_index'] = $page_no_index;
		$searchData['limit'] = $per_page;		
		$searchData['keyword'] = $keyword;	
		$searchData['sorting_order'] = $sorting_order; 
		$searchData['date'] = $date; 
	   	$config['base_url'] = site_url('admin/transactions/get_ajax_list?'.$sQuery);
		$total_rows = $this->transactions->countAllRecords($searchData);		
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$this->pagination->initialize($config);
		$paging = $this->pagination->create_links();
		$records = $this->transactions->getAllRecords($searchData);
		$output['alltransactiondetails'] = $records;
		
	    $html = $this->load->view('admin/transactions/ajax_list',$output,true);
	    $data['success'] = true;
	    $data['html'] = $html;	
	    $data['paging']	= $paging; 
	    echo json_encode($data); die;
	}
	
    function view($id)
    {
		$output['page_title'] = 'View transaction';
		$output['left_menu'] = 'Transactions_module';
		$output['left_submenu'] = 'Transactions';
        $record = $this->transactions->getSingleRecordById($id);
        $output['transactions'] = $record; 		    	
        $this->load->view('admin/includes/header',$output);
		$this->load->view('admin/transactions/view_transactions');
		$this->load->view('admin/includes/footer');
    }

    function delete()
	{
	  $id = $this->input->post('record_id');
	  $this->transactions->deleteRecordById($id);
      $data['success'] = true;
	  $data['message'] = 'Record deleted successfully';
	  $data['callBackFunction'] = 'callBackCommonDelete';
	  echo json_encode($data); die;
	}
	

   
}