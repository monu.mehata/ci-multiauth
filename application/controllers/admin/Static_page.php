<?php
/**
*Static_page Controller for admin
*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Static_page extends CI_Controller {
	
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->common_model->checkLoginAdminStatus();
		$this->load->model('admin/static_page_model', 'static_page');
	}
    
	function index()
	{
		$output['page_title'] = 'Site options module';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'static_page';
		$staticPages = $this->static_page->getAllStaticPages();
		$output['static_pages'] = $staticPages;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/static_page/static_page_list');
		$this->load->view('admin/includes/footer');
	}

	function changeStatus()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$this->static_page->changeStatusById($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated successfully';
		echo json_encode($data);
	}
    
	function multiTaskOperation()
	{
		$task = $this->input->post('task');
		$ids = $this->input->post('ids');
		$dataIds = explode(',',$ids);
		foreach ($dataIds as $key => $value) 
		{	
			if($task=='Active' || $task=='Inactive')
			{
				$this->static_page->changeStatusById($value,$task);			
				$message = 'Status of selected records changed successfully.';
			}
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}
	
    function update($id)
	{
		$output['page_title'] = 'Update static page';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'static_page';
		$output['message']    = '';
		$staticPage = $this->static_page->getSingleStaticPageById($id);
		checkRequestedDataExists($staticPage);
		$output['id'] = $staticPage->id;
		$output['title'] = $staticPage->title;
		$output['content'] = $staticPage->content;
		if(isset($_POST) && !empty($_POST))
		{
			$success = true;
			$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[250]');
			$this->form_validation->set_rules('content', 'Content', 'trim|required');
			if($this->form_validation->run())
			{
				$input = array();
				$input['title'] = $this->input->post('title');
				$input['content'] = $this->input->post('content');				
				$this->static_page->updateStaticPage($id,$input);
				$message = 'Record updated successfully';
				$success = true;
				$output['redirectURL'] = site_url('admin/static_page');
			}
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/static_page/static_page_form');
		$this->load->view('admin/includes/footer');
	}	
}