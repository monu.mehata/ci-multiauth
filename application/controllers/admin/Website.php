<?php
/**
* Website controller for Admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Website extends CI_Controller {
	
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->common_model->checkLoginAdminStatus();
		$this->load->model('admin/website_model', 'website');	}
 
	function index() {
		$output['page_title'] = "Site options module";
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'Website_setting';		
		$output['message']    = '';
		if ($_POST) {
			$this->form_validation->set_rules('smtp_host', 'SMTP host', 'trim|required');
			$this->form_validation->set_rules('smtp_port', 'SMTP post number', 'trim|required');
			$this->form_validation->set_rules('smtp_user', 'SMTP Username', 'trim|required');
			$this->form_validation->set_rules('smtp_pass', 'SMTP password', 'trim|required');
			$this->form_validation->set_rules('address', 'Addressk', 'required|trim');
			$this->form_validation->set_rules('contact_number', 'Contact number', 'required');			
			$this->form_validation->set_rules('contact_email', 'Contact email', 'required|trim|valid_email');
			$this->form_validation->set_rules('site_title', 'Website title', 'required|trim');
			$this->form_validation->set_rules('copyright_text', 'Copyright text', 'required|trim');

			$this->form_validation->set_rules('facebook_link', 'Facebook link', 'required|trim');			
			$this->form_validation->set_rules('google_plus_link', 'Google plus link', 'required|trim');
			$this->form_validation->set_rules('youtube_link', 'Youtube link', 'required|trim');
			$this->form_validation->set_rules('instagram_link', 'Instagram link', 'required|trim');
			$this->form_validation->set_rules('pinterest_link', 'Pinterest link', 'required|trim');
			$this->form_validation->set_rules('twitter_link', 'Twitter link', 'required|trim');
		

			if ($this->form_validation->run()) 
			{
				$options = $_POST;
				$this->website->updateOptionsSetting($options);			
				$message = 'Record updated successfully';
				$success = true;			
			}
			else
			{				
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}		
		$fields = array(                   
				'smtp_host',
				'smtp_port',
				'smtp_user',
				'smtp_pass',
				'copyright_text',
				'address',
				'contact_number',						
				'contact_email',				
				'suggestion_mail_to_users',
				'site_logo_name_front',
				'site_logo_name_admin',   				
				'site_title', 
				'facebook_link',				
				'google_plus_link',
				'youtube_link', 
				'instagram_link',
				'pinterest_link', 
				'twitter_link'								 
            );
        $options_data = array();     
        if(!empty($fields))
        {
            foreach ($fields as $value) 
            {
                $options_data[$value] = $this->website->getValueBySlug($value, true);
            }
        }
        $output['copyright_text'] = $this->common_model->getOptionValue('copyright_text');
        $output['site_title'] = $this->common_model->getOptionValue('site_title');
        $output['about_the_ioclothes'] = $this->common_model->getOptionValue('about_the_ioclothes');
        $output['options_content'] = (object) $options_data;
        if ($options_data['site_logo_name_front']) {
			$output['front_logo_image_url'] = site_url('/assets/uploads/logo/'.$options_data['site_logo_name_front']);
		}
		if ($options_data['site_logo_name_admin']) {
			$output['admin_logo_image_url'] = site_url('/assets/uploads/logo/'.$options_data['site_logo_name_admin']);
		}
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/setting/index');
		$this->load->view('admin/includes/footer');
	}
}