<?php
/*
Mail_templates Controller for Admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mail_templates extends CI_Controller {
	
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->common_model->checkLoginAdminStatus();
		$this->load->model('admin/mail_templates_model', 'mail_templates');
	}

	function index() {
		$output['page_title'] = 'Site options module';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'Mail_templates';
		$templates = $this->mail_templates->getAllTemplates();
		$output['templates'] = $templates;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_templates/mail_templates_list');
		$this->load->view('admin/includes/footer');
	}
 
	function changeStatus() {
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$this->mail_templates->changeStatusById($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated successfully';
		echo json_encode($data);
	}

	function multiTaskOperation() {
		$task = $this->input->post('task');
		$ids = $this->input->post('ids');
		$dataIds = explode(',',$ids);
		foreach ($dataIds as $key => $value) 
		{
			if($task=='Active' || $task=='Inactive')
			{
				$this->mail_templates->changeStatusById($value,$task);			
				$message = 'Status of selected records changed successfully.';
			}
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}
	
	function update($id) {
		$output['page_title'] = 'Update email template';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'Mail_templates';
		$output['message'] ='';
		$emailTemplate = $this->mail_templates->getSingleRecordById($id);
		checkRequestedDataExists($emailTemplate);
		$output['subject'] = $emailTemplate->subject; 
		$output['content'] = $emailTemplate->content;
		$output['name'] = $emailTemplate->name;
		if(isset($_POST) && !empty($_POST))
		{	
			$this->form_validation->set_rules('name', 'Template name', 'trim|required|max_length[250]');			
			$this->form_validation->set_rules('subject', 'Subject', 'trim|required|max_length[250]');
			$this->form_validation->set_rules('content', 'Content', 'trim|required');
			if($this->form_validation->run())
			{
				$input = array();
				$input['name'] = $this->input->post('name');
				$input['subject'] = $this->input->post('subject');
				$input['content'] = $this->input->post('content');
				$input['add_date'] = $this->common_model->getDefaultToGMTDate(time());		
				$this->mail_templates->updateEmailTemplate($id, $input);
				$message = 'Record updated successfully';
				$success = true;
				$output['redirectURL'] = site_url('admin/mail_templates');
			}	
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}
		$constants = array();
		$constantsB = explode(',',$emailTemplate->constants);
		foreach ($constantsB as $key => $value) 
		{
			$constants[$value] = '{{'.$value.'}}';
		}
		$output['constants'] = $constants;
		$output['id'] = $id;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/mail_templates/add_form');
		$this->load->view('admin/includes/footer');
	}
}