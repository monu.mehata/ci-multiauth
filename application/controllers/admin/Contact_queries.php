<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Contact_queries extends CI_Controller {
	function __construct() {
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->load->model('admin/Contact_queries_model', 'contact_queries');
		$this->load->model('mailsending_model', 'mailsend');		
	}

	function index()
	{
		$output['page_title']   = 'Site options module';
		$output['left_menu']    = 'Site_options';
		$output['left_submenu'] = 'Contact';
		$records = $this->contact_queries->getAllContactQueries();
		$output['allContact']   = $records;
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/contact_queries/contact_list');
		$this->load->view('admin/includes/footer');
	}

	function reply($id)
	{
		$output['page_title'] 	= 'Reply contact us';
		$output['left_menu'] 	= 'Site_options';
		$output['left_submenu'] = 'Contact';
		$input 					= array();
		$input['is_viewed']		= 'Yes';
		$this->contact_queries->updateContactQuery($id,$input);
		$contactQueries 		= $this->contact_queries->getSignleRecordById($id);
		checkRequestedDataExists($contactQueries);
		if(isset($_POST) && !empty($_POST))
		{
			$this->form_validation->set_rules('reply_message', 'Reply message', 'trim|required');
			if($this->form_validation->run())
			{
				$input = array();				
				$date_query = $this->input->post('date_query');				
				$input['reply_message'] = $this->input->post('reply_message');
				$input['is_replied']   = 'Yes';
				$input['is_viewed']    = 'Yes';
				$input['replied_date'] = $this->common_model->getDefaultToGMTDate(time());
				$this->contact_queries->updateContactQuery($id,$input);
				$replyMessage = $this->input->post('reply_message');
				$this->mailsend->replyContactQuery($replyMessage,$id);
			 	$message 			   = 'Reply message successfully sent';
				$success 			   = true;
				$output['redirectURL'] = site_url('admin/contact_queries');
			}
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}
		$output['contactQueries'] = $contactQueries;		
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/contact_queries/view_contact');
		$this->load->view('admin/includes/footer');
	}	

	function delete()
	{
		$id = $this->input->post('record_id');
		$this->contact_queries->deleteContactById($id);
		$data['success'] 		  = true;
		$data['message'] 		  = 'Record deleted successfully';
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}

	function multiTaskOperation()
	{
		$task 	 = $this->input->post('task');
		$ids 	 = $this->input->post('ids');
		$dataIds = explode(',',$ids);
		foreach ($dataIds as $key => $value) 
		{
			if($task == 'Delete')
			{
				$this->contact_queries->deleteContactById($value);
				$message = 'Selected record deleted successfully.';
			}
			
		}		
		$data['ids'] 	 = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}
}