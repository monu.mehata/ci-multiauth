<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Static_content extends CI_Controller 
{
	function __construct() 
	{
		Parent::__construct();
		$this->common_model->checkAdminLogin();
		$this->common_model->checkLoginAdminStatus();
		$this->load->model('admin/Static_content_model', 'static_content');		
	}

	function index(){
		$output['page_title'] = 'Site options module';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'static_content';
		$records = $this->static_content->getAllRecords();		   
		$output['allStaticContent'] = $records;  
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/static_content/static_content_list');
		$this->load->view('admin/includes/footer');
	}

	function update($id){
		$output['page_title'] = 'Update static content ';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'static_content';
		$output['message']    = '';
		$record = $this->static_content->getSingleRecordById($id);
		checkRequestedDataExists($record);
		$output['id'] = $record->id;
		$output['title'] = $record->title;
		$output['content'] = $record->content;
		$output['image_name'] = $record->image_name;		
		$output['status'] = $record->status;
		if(isset($_POST) && !empty($_POST))
		{			
			$success = true;	
			$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[250]');			
			if ($this->form_validation->run()) 
			{
				$input = array();				
				if(isset($_FILES['image_name']['name']) && $_FILES['image_name']['name']) 
				{
					$directory = './assets/uploads/static_content'; 
                    @mkdir($directory, 0777); 
                    @chmod($directory,  0777);  
                    $config['upload_path'] = $directory;
                    $config['allowed_types'] = 'gif|jpeg|jpg|png';           
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('image_name')) 
					{
						$image_data = $this->upload->data();
                        $file_name = $image_data['file_name'];
                        $input['image_name'] = $file_name;
					}
					else
					{
						$message = $this->upload->display_errors();
						$success = false;
					}
				}							
				if($success)
				{
					$input['title'] = $this->input->post('title');
					$input['content'] = $this->input->post('content');
					$input['status'] = $this->input->post('status');								
					$this->static_content->updateSingleRecordById($id,$input);
					$message = 'Record updated successfully';
					$output['redirectURL'] = site_url('admin/static_content');
					$success = true;
				}
			}
			else
			{
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}	
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/static_content/static_content_form');
		$this->load->view('admin/includes/footer');
	}	
	
	function changeStatus(){
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$this->static_content->changeStatusById($id,$status);
		$data['success'] = true;
		$data['message'] = 'Record updated successfully';
		echo json_encode($data);
	}

	function multiTaskOperation(){
		$task = $this->input->post('task');
		$ids = $this->input->post('ids');
		$dataIds = explode(',',$ids);
		foreach ($dataIds as $key => $value) 
		{
			if($task=='Active' || $task=='Inactive')
			{
				$this->static_content->changeStatusById($value,$task);			
				$message = 'Status of selected records changed successfully.';
			}
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}

	function view($id) {
		$output['page_title'] = 'View static content';
		$output['left_menu'] = 'Site_options';
		$output['left_submenu'] = 'static_content';
        $record = $this->static_content->getSingleRecordById($id);
        checkRequestedDataExists($record);
        $output['static_content'] = $record;    
        $this->load->view('admin/includes/header',$output);
		$this->load->view('admin/static_content/view_static_content');
		$this->load->view('admin/includes/footer');
    }
}