<?php
/**
* Admin controller for Admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	function __construct() {
		Parent::__construct();	
		$this->common_model->checkAdminLogin();	
		$this->load->model('admin/members_model', 'members');	 
	}

	function index() {		
		$output['page_title'] = "Dashboard";
		$output['left_menu'] = 'Dashboard';	
		$output['left_submenu'] = 'Dashboard';	
		$output['membersCount'] = $this->members->countAllRecordsOnSite();				  
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/admin/index');
		$this->load->view('admin/includes/footer');
		
	}	
}