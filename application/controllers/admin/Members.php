<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller 
{
	function __construct()
	{
	  Parent::__construct();
	  $this->load->model('admin/members_model','members');		
	}

	function index()
	{
	  $output['page_title'] = 'Members module';
	  $output['left_menu'] = 'Members_module';	
	  $output['left_submenu'] = 'Members';	
	  $record= $this->members->getAllrecord();
	  $output['alluser']= $record;
	  $this->load->view('admin/includes/header',$output);
	  $this->load->view('admin/members/members_list');
	  $this->load->view('admin/includes/footer');
	}

	function add() {
		$output['page_title'] = 'Add member';
		$output['left_menu'] = 'Members_module';
		$output['left_submenu'] = 'Members';	
		$output['message']    = '';
		$output['id'] = '';		
		$output['first_name'] = '';			
		$output['last_name'] = '';			
		$output['username'] = '';			
		$output['email'] = '';			
		$output['phone'] = '';			
		$output['password'] = '';			
		$output['status'] = 'Active';
		if(isset($_POST) && !empty($_POST)){			
			$this->form_validation->set_rules('first_name', 'First name', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('last_name', 'Last name', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('phone', 'Mobile number', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
			if ($this->form_validation->run()) {				
				$input = array();     				
                $input['first_name'] = $this->input->post('first_name');
                $input['last_name'] = $this->input->post('last_name');
                $input['username'] = $this->input->post('username');
                $input['email'] = $this->input->post('email');
                $input['phone'] = $this->input->post('phone');
                $input['password'] = $this->input->post('password');
               	$input['status'] = $this->input->post('status');								
				$input['add_date'] = $this->common_model->getDefaultToGMTDate(time());				
				$this->members->addNewRecord($input);
				$message = 'Record inserted successfully';
				$success = true;
				$output['redirectURL'] = site_url('admin/members');				        
			}
			else {
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}	
		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/members/members_form');
		$this->load->view('admin/includes/footer');
	}


	function update($id) {
		$output['page_title'] = 'Update member';
		$output['left_menu'] = 'Members_module';
		$output['left_submenu'] = 'Members';	
		$output['message']    = '';
		$record = $this->members->getSingleRecordById($id);
		$output['id'] = $record->id;	
		$output['first_name'] = $record->first_name;					
		$output['last_name'] = $record->last_name;					
		$output['username'] = $record->username;					
		$output['email'] = $record->email;					
		$output['phone'] = $record->phone;					
		$output['password'] = $record->password;					
		$output['status'] = $record->status;
		if(isset($_POST) && !empty($_POST)){			
			$success = true;				
			$this->form_validation->set_rules('first_name', 'First name', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('last_name', 'Last name', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('phone', 'Mobile number', 'trim|required|max_length[50]');
			if ($this->form_validation->run()) {
				$input = array();								
				if($success){			
				$password = $this->input->post('password');
					if($password){
						$salt = 'Ijxo1A16';
						$ency_password = md5(md5($password).md5($salt));
						$input['password'] = $ency_password;
					}					    	
                    $input['first_name'] = $this->input->post('first_name');	             
                    $input['last_name'] = $this->input->post('last_name');
                    $input['username'] = $this->input->post('username');	             
                    $input['email'] = $this->input->post('email');	                    
                    $input['phone'] = $this->input->post('phone');	                    
	                $input['status'] = $this->input->post('status');		
					$this->members->updateRecordById($id,$input);
					$message = 'Record updated successfully';
					$success = true;
					$output['redirectURL'] = site_url('admin/members');	
				}								
			}
			else {
				$success = false;
				$message = validation_errors();
			}
			$output['message'] = $message;
			$output['success'] = $success;
			echo json_encode($output);die;
		}	

		$this->load->view('admin/includes/header',$output);
		$this->load->view('admin/members/members_form');
		$this->load->view('admin/includes/footer');
	}

	function delete()
	{
	  $id = $this->input->post('record_id');
	  $this->members->deleteRecordById($id);
      $data['success'] = true;
	  $data['message'] = 'Record deleted successfully';
	  $data['callBackFunction'] = 'callBackCommonDelete';
	  echo json_encode($data); die;
	}


	function view($id)
	{
	  $output['page_title'] = 'View member';
	  $output['left_menu'] = 'Members_module';
	  $output['left_submenu'] = 'Members';	
	  $record = $this->members->getSingleRecordById($id);
	  $output['alluserview'] = $record;
	  $this->load->view('admin/includes/header',$output);
	  $this->load->view('admin/members/view_members');
	  $this->load->view('admin/includes/footer');
	}


	function multiTaskOperation()
	{
	   $task = $this->input->post('task');
	   $ids = $this->input->post('ids');
	   $dataIds = explode(',',$ids);
	   //pr($dataIds);die;
	   foreach ($dataIds as $key => $value)
	    {
		  if($task == 'Delete')
		  {
		    $this->members->deleteRecordById($value);
			$message = 'Selected record deleted successfully.';
		  }
		  else if ($task== 'Active' || $task== 'Inactive')
		  {
		  	$this->members->changeStatusById($value,$task);			
			$message = 'Status of selected records changed successfully.';
		  }
		}		
		$data['ids'] = $ids;
		$data['success'] = true;
		$data['message'] = $message;
		$data['callBackFunction'] = 'callBackCommonDelete';
		echo json_encode($data); die;
	}


	function changeStatus()
	{
	  $id = $this->input->get('id');
	  $status = $this->input->get('status');
	  $this->members->changeStatusById($id,$status);
	  $data['success'] = true;
	  $data['message'] = 'Record updated successfully';
	  echo json_encode($data);
	}


}	 