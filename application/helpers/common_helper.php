<?php 
if (!function_exists('pr')) {
    function pr($arr) {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }
}

if (!function_exists('showLimitedText')) {
    function showLimitedText($string,$len = 10) {
        $string = strip_tags($string);
        if (strlen($string) > $len)
            $string = mb_substr($string, 0, $len-3) . "...";
        return $string;
    }
}

if (!function_exists('getEmbedVideoURLByVideoURL')) {
    function getEmbedVideoURLByVideoURL($video_url) {
        $checkVimeo = checkVimeoVideo($video_url);
        $check = getDailyMotionId($video_url);
        if($check) {
          $video_array = explode('/',$video_url);
          $video_id = end($video_array);
          return "https://www.dailymotion.com/embed/video/".$video_id;
        } else if($checkVimeo) {
          $video_array = explode('/',$video_url);
          $video_id = end($video_array);
          return "https://player.vimeo.com/video/".$video_id;
        } else {
          $video_array = explode('=',$video_url);
          $video_id = end($video_array);
          return "https://www.youtube.com/embed/".$video_id;
        }
    }
}

if (!function_exists('checkVimeoVideo')) {
    function checkVimeoVideo($url) {
        $checkString = "/^.+vimeo.com\/?/";
        $m = preg_match($checkString,$url);
        return $m;
    }
}

if (!function_exists('getDailyMotionId')) {
    function getDailyMotionId($url) {
        $checkString = "/^.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/";
        $m = preg_match($checkString,$url);
        return $m;
    }
}

if (!function_exists('getTimeString')) {
    function getTimeString($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "Just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "1 minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "1 hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "Yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "1 week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "1 month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "1 year ago";
            }else{
                return "$years years ago";
            }
        }
        
    }
}

if (!function_exists('checkRequestedDataExists')) {
    function checkRequestedDataExists($data) {
        if(!$data) {
            show_404();
        }
        return true;
    }
}

if (!function_exists('createSlugForTable')) {
    function createSlugForTable($title,$table) {
        $slug = url_title($title);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params['slug'] = $slug;
        $CI = & get_instance(); 
        while ($CI->db->where($params)->get($table)->num_rows()) {
            if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }
            $params ['slug'] = $slug;
        }
        return $slug;
    }
}